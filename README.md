# Terraform PoC

This project is for learning terraform.

To create a new container I have read [docker_container documentation](https://www.terraform.io/docs/providers/docker/r/container.html).

I have modified container name because hello-world is smaller than ubuntu.

I have use a reference to the container_image to reuse that variable.

Every time I do:

    vagrant rsync

I have to do:

    terraform init

because .terraform is deleted.

Then, I do:

    terraform apply

to apply all changes.

terraform.tfstate should be saved into the repository, but this is a PoC!

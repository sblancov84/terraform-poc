provider "docker" {
    version = "2.6.0"
    host = "unix:///var/run/docker.sock"
}

resource "docker_image" "helloworld" {
    name = "hello-world:latest"
}

resource "docker_container" "hello-world" {
    image = docker_image.helloworld.name
    name = "terraform-demo"
}
